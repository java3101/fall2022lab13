import java.util.ArrayList;
import java.util.List;

public class ListPractice {
    // Counts the number of uppercase only strings inside an array
    public int countUpperCase(String[] strings) {
        int count = 0;
        for (String str : strings) {
            count += isUpperCase(str);
        }
        return count;
    }

    // Checks if a string is only uppercase
    public int isUpperCase(String str) {
        int count = 0;
        if (str.matches("^[A-Z]*$")) {
            count = 1;
        }
        return count;
    }

    // Returns a string with uppercase only strings
    public String[] getUpperCase(String[] strings) {
        int count = 0;
        for (String str : strings) {
            count += isUpperCase(str);
        }
        String[] newString = new String[count];
        int pos = 0;
        for (String str : strings) {
            if (str.matches("^[A-Z]*$")) {
                newString[pos] = str;
                pos++;
            }
        }
        return newString;
    }

    // Overloaded countUpperCase Method
    public int countUpperCase(List<String> words) {
        int count = 0;
        for (String str : words) {
            count += isUpperCase(str);
        }
        return count;
    }

    // Overloaded getUpperCase method
    public List<String> getUpperCase(List<String> words) {
        List<String> new_words = new ArrayList<>();
        for (String str : words) {
            if (str.matches("^[A-Z]*$")) {
                new_words.add(str);
            }
        }
        return new_words;
    }
}