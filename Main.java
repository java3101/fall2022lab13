import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        String[] strings = {"ABC","naME","HELLO","BYE","no"};
        
        ArrayList<String> words = new ArrayList<>();
        System.out.println(words.size());

        ArrayList<String> stillEmpty = new ArrayList<>(50);
        System.out.println(stillEmpty.size());

        for (String s : strings) {
            words.add(s);
        }
        System.out.println(words.size());

        System.out.println(words.contains("o"));
        System.out.println(words.contains("ABC"));

        ArrayList<Point> points = new ArrayList<>();
        Point point1 = new Point(2.0,3.0);
        Point point2 = new Point(2.0,3.0);
        Point point3 = new Point(2.0,3.0);

        points.add(point1);
        points.add(point2);
        points.add(point3);

        Point target = new Point(2.0,3.0);

        System.out.println(points.contains(target));
    }
}
