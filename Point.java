public class Point {
    public double x;
    public double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Point)) {
            return false;
        }
        Point object = (Point) other;
        return this.x == object.x && this.y == object.y;
    }
}
